#!/usr/bin/python

import os
import sys
import fileinput
import subprocess
import re

def main():

	print("", flush=True)
	os.system('clear')
	status = subprocess.check_output("oc status", shell=True).decode('utf-8')
	project = status.split("In project ",1)[1].split(' ')[0]
	print("Getting Project: " + project)

	print('\nSetup configmaps!')
	setup_configmaps()
	print('\nRun config files!')
	run_config_files()
	print('\nAdd \'name\' to service \'duke-shibboleth-test\' port to \'apacheport\'!')
	add_name_to_shib_port()
	print('\nSet triggers for duke-shibboleth-test deployment!')
	set_triggers_for_duke_shib(project)
	print('\nCreate empty shib-creds!')
	create_empty_shib_creds()
	print('\nCreate shib_creds with true values!')
	create_shib_creds()
	print('\nCreate Route!')
	create_route()
	print('\nFinal redeploy!')
	final_redeploy()

	print('\nDUKE SHIBBOLETH INSTALLATION COMPLETE\n\n')



def setup_configmaps():

	# ****************************************
	# *** setup apache-config/reqshib.conf ***
	# ****************************************

	global servername

	service_list = subprocess.check_output("oc get svc -o custom-columns=SERVICE:.metadata.name --no-headers=True", shell=True).decode('utf-8')

	services = re.split('\n', service_list)[:-1]

	if len(services) == 1:
		service_host_name = subprocess.check_output("oc get svc " + services[0] + " -o go-template --template='{{.metadata.name}}.{{.metadata.namespace}}.svc'", stderr=subprocess.STDOUT, shell=True).decode('utf-8')

	else:

		service_dict = {}

		print("\nSERVICES: ")

		for index, service in enumerate(services):
			print("{}. {}".format(index + 1, service))
			service_dict[index + 1] = service

		while True:

			try:
				service_number = int(input("Choose service number: "))
			except:
				continue

			if service_number in service_dict:
				service_host_name = subprocess.check_output("oc get svc " + service_dict[service_number] + " -o go-template --template='{{.metadata.name}}.{{.metadata.namespace}}.svc'", stderr=subprocess.STDOUT, shell=True).decode('utf-8')
				break



	print("Service Host Name: " + service_host_name)


	file_text_replace('apache-config/reqshib.conf', '%__SERVICE_NAME__%', service_host_name)




	# *******************************************
	# *** setup apache-config/servername.conf ***
	# *******************************************

	servername = input("\nEnter your planned URL for your site\nExample: https://example.cloud.duke.edu\n")

	file_text_replace('apache-config/servername.conf', '%__SERVER_NAME__%', servername)



	# *******************************************
	# *** setup apache-config/shibboleth2.xml ***
	# *******************************************

	file_text_replace('apache-config/shibboleth2.xml', '%__SERVER_NAME__%', servername)


	# *******************************************
	# *** setup shib-config/attribute-map.xml ***
	# *******************************************

	file_copy('shib-config/attribute-map.xml')


	# *****************************************
	# *** setup shib-config/shibboleth2.xml ***
	# *****************************************

	file_text_replace('shib-config/shibboleth2.xml', '%__SERVER_NAME__%', servername)


	# **************************
	# *** create config maps ***
	# **************************
	
	output_text = subprocess.check_output("oc create configmap apache-config \
		--from-file=reqshib.conf=./../config-maps-edited/apache-config/reqshib.conf \
		--from-file=servername.conf=./../config-maps-edited/apache-config/servername.conf \
		--from-file=shibboleth2.xml=./../config-maps-edited/apache-config/shibboleth2.xml \
		", shell=True).decode('utf-8')


	output_text = subprocess.check_output("oc create configmap shib-config \
		--from-file=attribute-map.xml=./../config-maps-edited/shib-config/attribute-map.xml \
		--from-file=shibboleth2.xml=./../config-maps-edited/shib-config/shibboleth2.xml \
		", shell=True).decode('utf-8')


def file_text_replace(filename, old_value, new_value):

	file_read = open('./../config-maps-templates/' + filename, 'r') 
	file_write = open('./../config-maps-edited/' + filename, 'w') 

	while True: 

		line = file_read.readline() 

		if not line: 
			break
		
		line = line.replace(old_value, new_value)

		file_write.write(line)
	  
	file_read.close() 
	file_write.close()


def file_copy(filename):

	file_read = open('./../config-maps-templates/' + filename, 'r') 
	file_write = open('./../config-maps-edited/' + filename, 'w') 

	while True: 

		line = file_read.readline() 

		if not line: 
			break
		
		file_write.write(line)
	  
	file_read.close() 
	file_write.close()

def run_config_files():

	# creagte images
	os.system('oc create -f ./../config/imageStream-centos.yaml')
	os.system('oc create -f ./../config/imageStream-httpd.yaml')
	os.system('oc create -f ./../config/imageStream-shibboleth.yaml')

	# create build configs
	os.system('oc create -f ./../config/buildConfig-httpd.yaml')
	os.system('oc create -f ./../config/buildConfig-shibboleth.yaml')

	# create deployment config
	os.system('oc create -f ./../config/deploymentConfig.yaml')

	# expose deployment config service
	os.system('oc expose dc/duke-shibboleth-test')



def add_name_to_shib_port():

	service = subprocess.check_output("oc get -o yaml service duke-shibboleth-test", shell=True).decode('utf-8')
	service_edit = re.sub('ports.*?selector','ports:\n    - name: apacheport\n      port: 8080\n      protocol: TCP\n      targetPort: 8080\n  selector',service, flags=re.DOTALL)
	service_file = open('./../temp/service_edit.yaml', 'w')
	service_file.write(service_edit)
	service_file.close()
	return_value = subprocess.check_output("oc delete service duke-shibboleth-test", shell=True).decode('utf-8')
	output = subprocess.check_output("oc create -f ./../temp/service_edit.yaml", shell=True).decode('utf-8')

def set_triggers_for_duke_shib(project):

	output = subprocess.check_output("oc set triggers dc/duke-shibboleth-test --from-image=" + project + "/duke-shibboleth-httpd:0.1 -c httpd", shell=True).decode('utf-8')
	output = subprocess.check_output("oc set triggers dc/duke-shibboleth-test --from-image=" + project + "/duke-shibboleth:0.1 -c shibd", shell=True).decode('utf-8')

def create_empty_shib_creds():

	# try:
	# 	is_secret = subprocess.check_output("oc get secret shib-creds", stderr=subprocess.STDOUT, shell=True).decode('utf-8')
	# except:
	# 	pass
	# else:
	# 	return_value = subprocess.check_output("oc delete secret shib-creds", shell=True).decode('utf-8')

	output_text = subprocess.check_output("oc create secret generic shib-creds \
		--from-file=tls.crt=./../tls_empty/tls.crt \
		--from-file=tls.key=./../tls_empty/tls.key \
		", shell=True).decode('utf-8')

	# *****************************
	# *** redeploy - not needed ***
	# *****************************
	return_value = subprocess.check_output("oc rollout status dc/duke-shibboleth-test", shell=True).decode('utf-8')
	# return_value = subprocess.check_output("oc rollout latest dc/duke-shibboleth-test", shell=True).decode('utf-8')
	



def create_shib_creds():

	pod_text = subprocess.check_output("oc get pods", shell=True).decode('utf-8')

	pod_list = pod_text.split('\n')

	running_shib_pod = ''

	for pod in pod_list:
		if 'Running' in pod and 'duke-shibboleth-test' in pod and not 'deploy' in pod:
			running_shib_pod = pod

	process = running_shib_pod.split(' ')[0]

	cert = subprocess.check_output('oc exec ' + str(process) + ' -c shibd cat /etc/shibboleth/sp-signing-cert.pem', shell=True).decode('utf-8')

	cert_file = open('./../tls_values/tls.crt', 'w') 
	cert_file.write(cert)
	cert_file.close() 



	key = subprocess.check_output('oc exec ' + str(process) + ' -c shibd cat /etc/shibboleth/sp-signing-key.pem', shell=True).decode('utf-8')

	key_file = open('./../tls_values/tls.key', 'w') 
	key_file.write(key)
	key_file.close() 



	try:
		is_secret = subprocess.check_output("oc get secret shib-creds", stderr=subprocess.STDOUT, shell=True).decode('utf-8')
	except:
		pass
	else:
		return_value = subprocess.check_output("oc delete secret shib-creds", shell=True).decode('utf-8')

	output_text = subprocess.check_output("oc create secret generic shib-creds \
		--from-file=tls.crt=./../tls_values/tls.crt \
		--from-file=tls.key=./../tls_values/tls.key \
		", shell=True).decode('utf-8')


	# print out cert
	print('\nThis is the certificate for response encryption at https://authentication.oit.duke.edu/manager/')
	print('Do not include "Begin Certificate" and "End Certificate" portions when copying.\n\n')
	print(cert)



def create_route():

	global servername

	hostname = remove_prefix(servername, "https://")

	return_value = subprocess.check_output('oc create route edge --service=duke-shibboleth-test \
		--insecure-policy=Redirect --hostname=' + str(hostname), shell=True).decode('utf-8')

	print(servername)


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

	
def final_redeploy():
	return_value = subprocess.check_output("oc rollout status dc/duke-shibboleth-test", shell=True).decode('utf-8')
	return_value = subprocess.check_output("oc rollout latest dc/duke-shibboleth-test", shell=True).decode('utf-8')
	





if __name__ == "__main__":
    main()








