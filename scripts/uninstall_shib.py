#!/usr/bin/python

import os
import sys
import fileinput
import subprocess
import re

def main():

	print("", flush=True)
	os.system('clear')

	output = subprocess.check_output("oc delete imagestream centos", shell=True).decode('utf-8')
	output = subprocess.check_output("oc delete imagestream duke-shibboleth", shell=True).decode('utf-8')
	output = subprocess.check_output("oc delete imagestream duke-shibboleth-httpd", shell=True).decode('utf-8')
	

	output = subprocess.check_output("oc delete buildconfig duke-shibboleth", shell=True).decode('utf-8')
	output = subprocess.check_output("oc delete buildconfig duke-shibboleth-httpd", shell=True).decode('utf-8')
	
	output = subprocess.check_output("oc delete deploymentconfig duke-shibboleth-test", shell=True).decode('utf-8')
	
	output = subprocess.check_output("oc delete service duke-shibboleth-test", shell=True).decode('utf-8')

	output = subprocess.check_output("oc delete configmap apache-config", shell=True).decode('utf-8')
	output = subprocess.check_output("oc delete configmap shib-config", shell=True).decode('utf-8')
	
	output = subprocess.check_output("oc delete secret shib-creds", shell=True).decode('utf-8')

	output = subprocess.check_output("oc delete routes duke-shibboleth-test", shell=True).decode('utf-8')


if __name__ == "__main__":
    main()