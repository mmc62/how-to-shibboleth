 
# <div align="center">How-To: Shibboleth</div>


#### Create Image Streams

	oc create -f config/imageStream-centos.yaml
	oc create -f config/imageStream-httpd.yaml
	oc create -f config/imageStream-shibboleth.yaml

#### Create Build Configs

	oc create -f config/buildConfig-httpd.yaml
	oc create -f config/buildConfig-shibboleth.yaml

#### Create Deployment Config

	oc create -f config/deploymentConfig.yaml

#### Expose Deployment Config Service

	oc expose dc/duke-shibboleth-test

---

## Edit config-maps

### Edit apache-config

#### reqshib.conf

	change line 8 and 9 (put in your apps service hostname exposed by application)

#### servername.conf

	change line 1 (put in your planned url for the site)

#### shibboleth2.xml

	change line 7 (put in your planned url for the site)

### Edit shib-config

#### attribute-map.xml

	comment or uncomment needed attributes to get from shib

#### shibboleth2.xml

	change line 7 (put in your planned url for the site) - same as above

## Add config maps to project

	apache-config
		reqshib.conf
		servername.conf
		shibboleth2.xml
	shib-config
		attribute-map.xml
		shibboleth2.xml

---

## Add name to Shib Port

	services -> duke-shibboleth-test -> edit yaml

	ports:
	    - name: apacheport
	      port: 8080
	      protocol: TCP
	      targetPort: 8080


## Change shibboleth deployment settings 

	Deployments -> duke-shibboleth-test -> Actions -> Edit

![setting](./images/deployment.png)



## Create Initial shib-creds Generic Secret

### shib-creds

	tls.crt - empty
	tls.key - empty

---
## re-deploy shibboleth
---

## Get true values for tls.crt and tls.key

delete shib-creds secret

get true tls.crt and tls.key values

	Pods -> <running duke shibboleth test pod> -> terminal -> choose shibd from dropdown

recreate shib-creds secret
	
	copy /etc/shibboleth/sp-signing-cert.pem to shib-creds -> tls.crt
	copy /etc/shibboleth/sp-signing-key.pen to shib-creds -> tls.key

---

## Create Route

	Hostname = (put in your planned url for the site)
	Service = duke-shibboleth-test
	Security
		Secure route (checked)
		tls Termination = Edge
		Insecure Traffic = Redirect














